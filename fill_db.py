from table_definition import Candidate, Companies, SkillCategories, EducationalInstitutions, \
    WorkExperience, WorkDetails, LanguageComputerSkills, SkillDescription, Education, EducationDescription, Interests


def create_candidate(session):
    candidate = Candidate("Martin", "Kavalír", "martinkavalir@seznam.cz", 1)
    session.add(candidate)
    session.commit()


def create_companies(session):
    company_1 = Companies("SMC Industrial Automation", "Industrial Automation", 20853, "https://www.smc.eu/", 1)
    company_2 = Companies("Immunotech", "Biotechnology", 100, "http://immunotech.cz/", 2)
    session.add_all([company_1, company_2])
    session.commit()


def create_skill_categories(session):
    skill_category_1 = SkillCategories("language", 1)
    skill_category_2 = SkillCategories("computer", 2)
    session.add_all([skill_category_1, skill_category_2])
    session.commit()


def create_educational_institution(session):
    institution_1 = EducationalInstitutions("University of Chemistry and Technology", "University", 1)
    institution_2 = EducationalInstitutions("Dr. Josef Pekař Gymnazium", "Gymnasium", 2)
    session.add_all([institution_1, institution_2])
    session.commit()


def fill_work_experience(session):
    we_1 = WorkExperience(1, "freelancer", "natural gas trading software", "2020-01-01", we_id=3)
    we_2 = WorkExperience(1, "employee", "Application Engineer", "2016-12-01", "2019-12-31", 1, 2)
    we_3 = WorkExperience(1, "employee", "Production Planner", "2016-01-01", "2016-11-30", 2, 1)
    session.add_all([we_1, we_2, we_3])
    session.commit()


def fill_work_details(session):
    wd_1 = WorkDetails(1, "Planning and optimization of production")
    wd_2 = WorkDetails(1, "Planning and optimization of raw materials purchase")
    wd_3 = WorkDetails(2, "Leading the developement of compresed air treatment equipment for ELI Beamlines")
    wd_4 = WorkDetails(2, "Providing technical support for external and internal clients")
    wd_5 = WorkDetails(2, "Working on diverse projects in semidonductor, chemical and automotive industry")
    wd_6 = WorkDetails(3, "Business analysis of natural gas market")
    wd_7 = WorkDetails(3, "Identification of all needed data sources and algorithm creation")
    wd_8 = WorkDetails(3, "Software design - used technologies - Python, JavaScript, MySQL")
    wd_9 = WorkDetails(3, "Programming")
    wd_10 = WorkDetails(3, "Testing")
    wd_11 = WorkDetails(3, "Making business decision based on results generated by prediction model")
    wd_12 = WorkDetails(3, "Sample programming code - https://gitlab.com/martinkavalir/natural_gas_prediction_model")
    session.add_all([wd_1, wd_2, wd_3, wd_4, wd_5, wd_6, wd_7, wd_8, wd_9, wd_10, wd_11, wd_12])
    session.commit()


def fill_lang_comp_skills(session):
    lcs_1 = LanguageComputerSkills("English", 1, 1, 1)
    lcs_2 = LanguageComputerSkills("German", 1, 1, 2)
    lcs_3 = LanguageComputerSkills("Python", 2, 1, 3)
    lcs_4 = LanguageComputerSkills("MySQL", 2, 1, 4)
    lcs_5 = LanguageComputerSkills("VBA", 2, 1, 5)
    lcs_6 = LanguageComputerSkills("Word", 2, 1, 6)
    lcs_7 = LanguageComputerSkills("Excel", 2, 1, 7)
    lcs_8 = LanguageComputerSkills("PowerPoint", 2, 1, 8)
    lcs_9 = LanguageComputerSkills("Autodesk Fusion 360", 2, 1, 9)
    session.add_all([lcs_1, lcs_2, lcs_3, lcs_4, lcs_5, lcs_6, lcs_7, lcs_8, lcs_9])
    session.commit()


def fill_lang_comp_descr(session):
    lcd_1 = SkillDescription(1, "3 years of experience in multinational team with daily English communication", "active")
    lcd_2 = SkillDescription(2, "Passive knowledge", "active")
    lcd_3 = SkillDescription(3, "1+ year experince, main focus data processing and analysis", "active", "Programming Language")
    lcd_4 = SkillDescription(4, "1+ year experince", "active", "Programming Language")
    lcd_5 = SkillDescription(5, "1 year experince, main focus data processing and analysis", "active", "Programming Language")
    lcd_6 = SkillDescription(6, "Expert knowledge", "active", "MS Office")
    lcd_7 = SkillDescription(7, "Expert knowledge", "active", "MS Office")
    lcd_8 = SkillDescription(8, "Expert knowledge", "active", "MS Office")
    lcd_9 = SkillDescription(9, "2+ years experince", "active", "3D software")
    session.add_all([lcd_1, lcd_2, lcd_3, lcd_4, lcd_5, lcd_6, lcd_7, lcd_8, lcd_9])
    session.commit()


def fill_education(session):
    e_1 = Education("Innovation Project Management", 1, "2010-09-01", "2015-06-30", 'active', 1, 1)
    e_2 = Education("General", 1, "2006-09-01", "2010-06-30", 'inactive', 2, 2)
    session.add_all([e_1, e_2])
    session.commit()


def fill_education_description(session):
    ed_1 = EducationDescription(1, 'Subject', "Mathematics and Statistics")
    ed_2 = EducationDescription(1, 'Subject', "Quantitative methods")
    ed_3 = EducationDescription(1, 'Subject', "Economics")
    ed_4 = EducationDescription(1, 'Subject', "Chemical Engineering")
    ed_5 = EducationDescription(1, 'Master Thesis', "Innovative application of recycled materials in automotive (Škoda Auto)")
    ed_6 = EducationDescription(1, 'Bachelor Thesis', "Evaluation of economic efficiency of hydrogen production (Česká Rafinérská)")
    session.add_all([ed_1, ed_2, ed_3, ed_4, ed_5, ed_6])
    session.commit()


def fill_interests(session):
    i_1 = Interests("Machine Learning", 1, "active")
    i_2 = Interests("Golf", 1, "active")
    i_3 = Interests("Joinery", 1, "active")
    i_4 = Interests("Trading", 1, "active")
    session.add_all([i_1, i_2, i_3, i_4])
    session.commit()
