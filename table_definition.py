from sqlalchemy.orm import declarative_base

from sqlalchemy import Column, String, Integer, Date, Enum, ForeignKey
from sqlalchemy.orm import relationship


BASE = declarative_base()


class Candidate(BASE):
    __tablename__ = 'candidate'
    c_id = Column("id", Integer, primary_key=True)
    first_name = Column("first_name", String)
    surname = Column("surname", String)
    email = Column("email", String)
    work_experiences = relationship("WorkExperience")
    language_computer_skills = relationship("LanguageComputerSkills")
    education = relationship("Education")
    interests = relationship("Interests")

    def __init__(self, first_name, surname, email, c_id=None):
        self.c_id = c_id
        self.first_name = first_name
        self.surname = surname
        self.email = email


class WorkExperience(BASE):
    __tablename__ = 'work_experience'
    we_id = Column("id", Integer, primary_key=True)
    candidate_id = Column("candidate_id", ForeignKey('candidate.id'), Integer)
    work_type = Column("work_type", String)
    job_title = Column("job_title", String)
    company_id = Column("company_id", ForeignKey('company.id'), Integer, primary_key=True)
    contract_start = Column("contract_start", Date)
    contract_end = Column("contract_end", Date)
    company = relationship("Companies")
    work_details = relationship("WorkDetails")

    def __init__(self, candidate_id, work_type, job_title, contract_start, contract_end=None, company_id=None, we_id=None):
        self.candidate_id = candidate_id
        self.work_type = work_type
        self.job_title = job_title
        self.company_id = company_id
        self.contract_end = contract_end
        self.contract_start = contract_start
        self.we_id = we_id


class WorkDetails(BASE):
    """V této tabulce se budou přidávat jednotlivé záznamy k dané work_experience"""
    id = Column("id", Integer, primary_key=True)
    work_experience = Column("work_experience_id", ForeignKey('work_experience.id'), Integer, primary_key=True)
    work_detail = Column("work_detail", String, primary_key=True)

    def __init__(self, work_experience, work_detail):
        self.work_experience = work_experience
        self.work_detail = work_detail


class Companies(BASE):
    __tablename__ = 'company'
    c_id = Column("id", Integer, primary_key=True)
    company_name = Column("company_name", String)
    scope_of_business = Column("scope_of_business", String)
    company_size = Column("company_size", Integer)
    company_website = Column("company_web", String)

    def __init__(self, company_name, scope_of_business=None, company_size=None, company_website=None, c_id=None):
        self.company_name = company_name
        self.scope_of_business = scope_of_business
        self.company_size = company_size
        self.company_website = company_website
        self.c_id = c_id


class LanguageComputerSkills(BASE):
    """Vymyslet strukturu tabulky z CV"""
    __tablename__ = 'language_computer_skill'
    lcs_id = Column("id", Integer, primary_key=True)
    skill_name = Column("skill_name", String, unique=True)
    skill_category_id = Column("skill_category_id", ForeignKey("skill_category.id"), Integer)
    candidate_id = Column("candidate_id", ForeignKey('candidate.id'), Integer)
    skill_descriptions = relationship("SkillDescription")

    def __init__(self, skill_name, skill_category_id, candidate_id, lcs_id=None):
        self.skill_name = skill_name
        self.skill_category_id = skill_category_id
        self.candidate_id = candidate_id
        self.lcs_id = lcs_id


class SkillCategories(BASE):
    __tablename__ = 'skill_category'
    s_id = Column("id", Integer, primary_key=True)
    category_name = Column("category_name", String, unique=True)

    def __init__(self, category_name, s_id=None):
        self.category_name = category_name
        self.s_id = s_id


class SkillDescription(BASE):
    __tablename__ = 'skill_description'
    id = Column("id", Integer, primary_key=True)
    language_computer_skills_id = Column("lang_comp_skill_id", ForeignKey("language_computer_skill.id"), Integer, primary_key=True)
    skill_type = Column("skill_type", String)
    description = Column("description", String)
    status = Column("status", Enum('active', 'inactive'))

    def __init__(self, language_computer_skills_id, description, status, skill_type=None):
        self.language_computer_skills_id = language_computer_skills_id
        self.description = description
        self.status = status
        self.skill_type = skill_type


class Education(BASE):
    __tablename__ = 'education'
    e_id = Column("id", Integer, primary_key=True)
    education_name = Column("education_name", String)
    ed_institution_id = Column("ed_institution_id", ForeignKey("educational_institution.id"), Integer)
    education_start = Column("education_start", Date)
    education_end = Column("education_end", Date)
    status = Column("status", Enum('active', 'inactive'))
    candidate_id = Column("candidate_id", ForeignKey('candidate.id'), Integer)
    education_descriptons = relationship("EducationDescription")

    def __init__(self, education_name, ed_institution_id, education_start, education_end, status, candidate_id, e_id=None):
        self.education_name = education_name
        self.ed_institution_id = ed_institution_id
        self.education_start = education_start
        self.education_end = education_end
        self.status = status
        self.candidate_id = candidate_id
        self.e_id = e_id


class EducationDescription(BASE):
    __tablename__ = 'education_descripton'
    id = Column("id", Integer, primary_key=True)
    education_id = Column("education_id", ForeignKey("education.id"), Integer, primary_key=True)
    education_subject = Column("education_subject", String)
    subject_type = Column("subject_type", Enum('Subject', 'Master Thesis', 'Bachelor Thesis'))

    def __init__(self, education_id, subject_type, education_subject):
        self.education_id = education_id
        self.subject_type = subject_type
        self.education_subject = education_subject


class EducationalInstitutions(BASE):
    __tablename__ = 'educational_institution'
    e_id = Column("id", Integer, primary_key=True)
    institution_name = Column("institution_name", String, unique=True)
    institution_type = Column("institution_type", String)

    def __init__(self, institution_name, institution_type, e_id=None):
        self.institution_name = institution_name
        self.institution_type = institution_type
        self.e_id = e_id


class Interests(BASE):
    __tablename__ = 'interests'
    id = Column("id", Integer, primary_key=True)
    interest_name = Column("interest_name", String)
    candidate_id = Column("candidate_id", ForeignKey('candidate.id'), Integer)
    status = Column("status", Enum('active', 'inactive'))

    def __init__(self, interest_name, candidate_id, status):
        self.interest_name = interest_name
        self.candidate_id = candidate_id
        self.status = status
