from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from fill_db import create_candidate, create_companies, create_skill_categories, \
    create_educational_institution, fill_work_experience, fill_work_details, \
    fill_lang_comp_skills, fill_lang_comp_descr, fill_education, fill_education_description, \
    fill_interests

engine = create_engine("sqlite:///candidate_db.db")
session = sessionmaker(bind=engine)()

create_candidate(session)
create_companies(session)
create_skill_categories(session)
create_educational_institution(session)
fill_work_experience(session)
fill_work_details(session)
fill_lang_comp_skills(session)
fill_lang_comp_descr(session)
fill_education(session)
fill_education_description(session)
fill_interests(session)

