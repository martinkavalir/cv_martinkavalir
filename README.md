This is single-purpose program which is used for presentation of options for automatically generated documents such as reports. 
This particular example reates CV in pdf. There is possibility to choose from 106 languages.
On backend there is created a database with all information needed for CV. There is strong focus to integrity of database.
After filling database with data it selects all relevant information and creates pdf to predefined template.